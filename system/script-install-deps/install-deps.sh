#!/bin/bash
set -euo pipefail
set -x

export PATH="/app/debuerreotype/scripts/:$PATH"

cd /task

mkdir -p /task/chroot/usr/share/man/man1
debuerreotype-apt-get ./chroot update -qq
debuerreotype-apt-get ./chroot install -y python3 apksigner curl lib32gcc1 lib32ncurses5 lib32stdc++6 lib32z1 default-jdk-headless python3-pil python3-pyasn1 python3-pyasn1-modules python3-ruamel.yaml python3-yaml unzip
