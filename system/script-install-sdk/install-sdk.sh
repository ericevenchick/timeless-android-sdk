#!/bin/bash
set -euo pipefail
set -x

mkdir -p /opt/android-sdk-linux
cd /opt/android-sdk-linux
wget https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip
unzip sdk-tools-linux-4333796.zip

export USE_SDK_WRAPPER=yes
export ANDROID_HOME=/opt/android-sdk-linux
export PATH=$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/tools/bin:$ANDROID_HOME/platform-tools
yes | sdkmanager --licenses || true 
sdkmanager "platform-tools" "platforms;android-28"

cd /opt
wget https://downloads.gradle-dn.com/distributions/gradle-5.6.4-bin.zip
unzip gradle-5.6.4-bin.zip
export GRADLE_OPTS=-Dorg.gradle.daemon=false 
/opt/gradle-5.6.4/bin/gradle
