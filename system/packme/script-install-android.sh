#!/bin/bash
set -euo pipefail
set -x

export PATH="/app/debuerreotype/scripts/:$PATH"

cd /task

mkdir -p /task/chroot/usr/share/man/man1
apt-get update
apt-get install unzip
debuerreotype-apt-get ./chroot update -qq
debuerreotype-apt-get ./chroot install -y python3 apksigner curl lib32gcc1 lib32ncurses5 lib32stdc++6 lib32z1 default-jdk-headless python3-pil python3-pyasn1 python3-pyasn1-modules python3-ruamel.yaml python3-yaml

mkdir -p /task/chroot/opt/android-sdk-linux
cd /task/chroot/opt/android-sdk-linux
wget https://dl.google.com/android/repository/tools_r25.2.3-linux.zip
echo 'aafe7f28ac51549784efc2f3bdfc620be8a08213  tools_r25.2.3-linux.zip' | sha1sum -c
unzip tools_r25.2.3-linux.zip
chroot /task/chroot
export USE_SDK_WRAPPER=yes
export ANDROID_HOME=/opt/android-sdk-linux
export PATH=$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools
(while sleep 3; do echo 'y'; done) | /opt/android-sdk-linux/tools/android update sdk --no-ui --filter platform-tools,tools,build-tools-25.0.2,android-24
exit 
